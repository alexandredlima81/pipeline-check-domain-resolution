#!/bin/bash

domain="agrotis.local"
nslookup_output=$(dig +short $domain)

if [ -z "$nslookup_output" ]; then
    echo "Status de validação: Falha ao resolver o domínio."
    systemctl restart named
    sleep 5
    nslookup_output=$(dig +short $domain)
    if [ -z "$nslookup_output" ]; then
        echo "Status de validação: Falha ao resolver o domínio mesmo após reiniciar o serviço named."
        exit 2
    else
        echo "Status de validação: Domínio resolvido após reiniciar o serviço named."
        exit 1
    fi
else
    echo "Status de validação: Domínio resolvido com sucesso."
    exit 0
fi
